#!/bin/bash
mkdir -p ~/.ssh
cp id_rsa ~/.ssh/
chmod 600 ~/.ssh/id_rsa
echo -e "Host *\n   StrictHostKeyChecking no\n   UserKnownHostsFile=/dev/null" > ~/.ssh/config
git clone https://github.com/amazon-connect/amazon-connect-chat-ui-examples.git
cd amazon-connect-chat-ui-examples/
git remote rename origin old-origin
git remote add origin git@gitlab.com:shubham.kalsait/github-chatapi.git
git push -u origin --all
git push -u origin --tags