#!/bin/bash

apt-get install wget unzip -y
wget https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -O "awscliv2.zip"
unzip awscliv2.zip
./aws/install
export PATH=$PATH:/usr/local/bin/